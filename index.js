const express=require("express")
const bodyParser=require("body-parser")

const app=express()
app.use(bodyParser.json())

app.post("/v1/getdata",async(req,res)=>{
    res.send({message:"SentData"})
});

app.listen(3000,()=>{
    console.log("server started")
})